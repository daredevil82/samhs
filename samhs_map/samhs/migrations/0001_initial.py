# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='County',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('county_name', models.CharField(unique=True, max_length=25)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CountyLocation',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('county', models.ForeignKey(to='samhs.County')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Location',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('type', models.CharField(max_length=3)),
                ('name', models.CharField(unique=True, max_length=50)),
                ('address', models.CharField(max_length=255)),
                ('latitude', models.DecimalField(max_digits=8, decimal_places=6)),
                ('longitude', models.DecimalField(max_digits=8, decimal_places=6)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='countylocation',
            name='location',
            field=models.ForeignKey(to='samhs.Location'),
            preserve_default=True,
        ),
    ]
