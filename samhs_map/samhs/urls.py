from django.conf.urls import patterns, url

from samhs import views

urlpatterns = patterns('',
	url(r'^$', views.index, name='index'),
    url(r'^location/save$', views.locationSave),
    url(r'^location/get$', views.locationGetAll),
    url(r'^location/delete$', views.locationDelete),
    url(r'^user/login$', views.userLogin),
    url(r'^user/logout', views.userLogout),
    url(r'^user/status', views.userStatus)
)
