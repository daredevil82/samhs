from decimal import Decimal
import json

from django.contrib.auth import authenticate, login, logout
from django.db import transaction, IntegrityError
from django.http import HttpResponse
from django.shortcuts import render

from models import County, Location, CountyLocation

def index(request):
    return render(request, "samhs/index.html")

def userLogin(request):
    if request.method == "POST":
        data = json.loads(request.POST.get("data", None))

        if data is not None:
            user = authenticate(username = data["name"], password = data["password"])

            if user is not None:
                login(request, user)
                return HttpResponse(json.dumps({"success" : "true"}), content_type = "application/json")
            else:
                return HttpResponse(json.dumps({"success" : "false", "message" : "invalid_user"}), 
                                    content_type = "application/json")
        else:
            return HttpResponse(json.dumps({"success" : "false", "message" : "invalid_request"}), 
                                content_type = "application/json")

def userLogout(request):
    logout(request)
    return HttpResponse(json.dumps({"success" : "true"}), content_type = "application/json")

def userStatus(request):
    if request.user.is_authenticated():
        return HttpResponse(json.dumps({"success" : "true", "login" : "true"}), content_type = "application/json")
    else:
        return HttpResponse(json.dumps({"success" : "true", "login" : "false"}), content_type = "application/json")


def locationGetAll(request):
    locationJson = []

    for l in CountyLocation.objects.select_related().all():
         locationJson.append({"county" : l.county.county_name, "id" : int(l.location.id), "type" : l.location.type, 
                    "name" : l.location.name, "address" : l.location.address, "lat" : l.location.latitude,
                    "lng" : l.location.longitude})

    return HttpResponse(json.dumps(locationJson, default=decimal_default), content_type = "application/json")


def locationSave(request):
    if request.method == "POST" and request.user.is_authenticated():
        data = json.loads(request.POST.get("location-data", None))
        print data

        thisCounty = County.objects.get(county_name = data["county_name"])
        countyLocation = None
        try:

            countyLocation = Location.objects.get(id = data["locationId"])
            print "Updating current location"
            countyLocation = populateLocation(data,countyLocation)
            countyLocation.save()

        except Location.DoesNotExist:

            print "Creating new location"

            try:
                newLocation = Location.objects.create(name = data["name"], 
                                                        latitude = data["lat"], longitude = data["long"],
                                                        address = data["address"], type = data["type"])
            
                countyLocation = CountyLocation(county = thisCounty, location = newLocation)
                countyLocation.save()

            except IntegrityError as e:
                return HttpResponse(json.dumps({"success" : "false", "message" : "Duplicate location name"}),
                                    content_type = "application/json")

        return HttpResponse(json.dumps({"success" : "true", "location" : countyLocation.location.id}),
                                content_type = "application/json")
    else:
        return HttpResponse(json.dumps({"success" : "false", "message" : "invalid_request"}), 
                                content_type = "application/json")

def locationDelete(request):
    if request.method == "POST" and request.user.is_authenticated():
        data = json.loads(request.POST.get("data", None))

        if data is not None:
            loc = Location.objects.get(id = data["id"])
            countyLocation = CountyLocation.objects.get(location = loc)
            countyLocationId = countyLocation.id

            countyLocation.delete()
            loc.delete()

            try:
                countyLocation = CountyLocation.objects.get(id = countyLocationId)
                return HttpResponse(json.dumps({"success" : "false", "message" : 
                                    "Error deleting CountyLocation reference" + data["id"]}), 
                                    content_type = "application/json")

            except CountyLocation.DoesNotExist, KeyError:
                return HttpResponse(json.dumps({"success" : "true"}), content_type = "application/json")
    else:
        return HttpResponse(json.dumps({"success" : "false", "message" : "invalid_request"}), 
                                content_type = "application/json")


def populateLocation(data, location):
    location.name = data["name"]
    location.address = data["address"]
    location.latitude = data["lat"]
    location.longitude = data["long"]
    location.type = data["type"]

    return location

#handler to convert Decimal fieldtype to floats, since Decimal types are not JSON serializable
def decimal_default(obj):
    if isinstance(obj, Decimal):
        return float(obj)
    raise TypeError

