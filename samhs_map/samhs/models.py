from django.db import models

# Create your models here.

class County(models.Model):
    id = models.AutoField(primary_key=True)
    county_name = models.CharField(max_length=25, unique=True)

class Location(models.Model):
    id = models.AutoField(primary_key=True)
    type = models.CharField(max_length=3)
    name = models.CharField(max_length=50, unique=True)
    address = models.CharField(max_length=255)
    latitude = models.DecimalField(max_digits=8, decimal_places=6)
    longitude = models.DecimalField(max_digits=8, decimal_places=6)

class CountyLocation(models.Model):
    id = models.AutoField(primary_key=True)
    county = models.ForeignKey(County)
    location = models.ForeignKey(Location)