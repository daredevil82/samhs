/**
 * Created by jasonjohns on 9/8/14.
 */

"use strict";
var opts = {
    mapDefaults : {
        maineLat :  45.258225,
        maineLong : -69.115966, 
        zoom :      7,
        minZoom :   7,
        mapType :   google.maps.MapTypeId.ROADMAP
    },        
    geocoder :  null,
    map :       null,
    markers :   [],
    markerSelect : {
        id : -1
    },
    infoWindow : null,
    infoWindowDom : "<h2 class = 'window window-name'>%s</h2>" +
                    "<p class = 'window window-address'>Address: %s</p>" +
                    "<p class = 'window window-type'>Location Type: %s</p>",
    login : false
};

var elems = {};

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

function init() {
    var mapOptions = {
        center: new google.maps.LatLng(opts.mapDefaults.maineLat, opts.mapDefaults.maineLong),
        zoom: opts.mapDefaults.zoom,
        minZoom: opts.mapDefaults.minZoom,
        mapTypeId: opts.mapDefaults.mapType
    };
    opts.geocoder = new google.maps.Geocoder();
    opts.map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
    opts.infoWindow = new google.maps.InfoWindow();

    retrieveAllLocations();
    checkLogin();
}

//Executes on load, adds all locations to map
function retrieveAllLocations(){
    $.ajax({
        url : "/location/get",
        type : "GET",
        dataType : "json",
        success : function(data, status, xhr){
            for (var i = 0; i < data.length; i++){

                var marker = new google.maps.Marker({
                    map : opts.map,
                    position : new google.maps.LatLng(data[i].lat, data[i].lng),
                    id : data[i].id,
                    name : data[i].name,
                    address : data[i].address,
                    type : data[i].type,
                });

                opts.markers[marker.id] = marker;

                google.maps.event.addListener(marker, "click", function(e){
                    markerClickHandler(this, e);
                });
            }
        }
    });
}

//need a non-asynchronous function to save the most recent set of coordinates returned from the Geocode API
function saveLocation(location, marker){
    var newLocationLat = location.geometry.location.lat(),
        newLocationLong = location.geometry.location.lng(),
        county = location.address_components[3].long_name.split(" County")[0];

    $.ajax({
        url : "/location/save",
        type : "POST",
        data : { "location-data": JSON.stringify({
                        lat: newLocationLat,
                        long: newLocationLong,
                        county_name: county,
                        address: location.formatted_address,
                        name: elems.nameInput.val(),
                        type: elems.typeInput.val(),
                        locationId : opts.markerSelect.id
                    })
                },
        dataType : "json",
        success : function(data, status, xhr){
            if (data.success === "true"){
                marker.id = data.location;
                marker.name = elems.nameInput.val();
                marker.type = elems.typeInput.val();
                marker.address = elems.addressInput.val();

                opts.markers[marker.id] = marker;
                opts.markerSelect = marker;
            } else {
                opts.marker.setMap(null);
                opts.markerSelect = {
                    id : -1
                };
            }

            elems.deleteButton.prop("disabled", false);

            google.maps.event.addListener(marker, "click", function(e){
                markerClickHandler(this, e);
            });
        }
    });
}

function deleteLocation(){
    $.ajax({
        url : "/location/delete",
        type : "POST",
        data : {"data" : JSON.stringify({"id" : opts.markerSelect.id})},
        dataType : "json",
        success : function(data, status, xhr){
            if (data.success === "true"){
                opts.markers[opts.markerSelect.id].setMap(null);
                opts.markers[opts.markerSelect.id] = null;
            }
            else {
                console.log("Delete Failed");
            }
        }
    });
}

function checkLogin(){
    $.ajax({
        url : "/user/status",
        type : "GET",
        dataType : "json",
        success : function(data, status, xhr){
            console.log(status);

            if (data.success === "true" && data.login === "true"){
                elems.entryWrapper.removeClass('hidden');
                opts.login = true;
            }
            else if (data.login === "false"){
                elems.entryWrapper.addClass('hidden');
                opts.login = false;
            }
        }
    });
}

function loginUser(){
    var username = elems.userInput.val(),
        password = elems.passwordInput.val();
    $.ajax({
        url : "/user/login",
        type : "POST",
        data : {"data" : JSON.stringify({"name" : username, "password" : password})},
        dataType : "json",
        success : function(data, status, xhr){
            console.log(status);

            if (data.success === "true"){
                $("#login-modal").modal("toggle");-
                elems.entryWrapper.removeClass("hidden");
                opts.login = true;
            }
            else {
                elems.entryWrapper.addClass('hidden');
                opts.login = false;
            }
        }
    })
}

function logoutUser(){
    $.ajax({
        url : "/user/logout",
        type : "GET",
        success : function(data, status, xhr){
            if (data.success === "true") {
                opts.login = false;
                elems.entryWrapper.addClass('hidden');
            }
        }
    })
}

//clear all data input fields and close infowindow
function clearFields(){
    opts.markerSelect = {
        id : -1
    };

    elems.nameInput.val("");
    elems.addressInput.val("");
    opts.infoWindow.close();

}

//use Maps Geocode API to translate address to GPS coordinates
function coordinateLookup(results, status) {
    elems.errorDisplay.text("");

    if (status === google.maps.GeocoderStatus.OK) {
        opts.map.setCenter(results[0].geometry.location);
        var marker = new google.maps.Marker({
            map: opts.map,
            position: results[0].geometry.location
        });
        saveLocation(results[0], marker)
    } else {
        elems.errorDisplay.text(status);
    }
}

// handles marker click events by populating infowindow object and data input fields
function markerClickHandler(thisEvent, e){

    opts.markerSelect = thisEvent;
    opts.infoWindow.close();


    var infoString = sprintf(opts.infoWindowDom, opts.markerSelect.name, opts.markerSelect.address, opts.markerSelect.type);

    elems.nameInput.val(opts.markerSelect.name);
    elems.addressInput.val(opts.markerSelect.address);

    elems.deleteButton.prop("disabled", false);
    elems.clearButton.prop("disabled", false);

    opts.infoWindow.setContent(infoString);
    opts.infoWindow.open(opts.map, opts.markers[opts.markerSelect.id]);

}

jQuery(document).ready(function() {

    elems = {
        nameInput :     $("#location-name"),
        addressInput :  $("#location-address"),
        typeInput :     $("#location-type"),
        saveButton :    $("#location-submit"),
        deleteButton :  $("#location-delete"),
        clearButton :   $("#location-clear"),
        errorDisplay :  $("#geocode-error"),
        userInput :     $("#user-login-name"),
        passwordInput : $("#user-login-password"),
        loginButton :   $("#user-login"),
        logoutButton :  $("#btn-logout"),
        entryWrapper :  $("#location-entry")
    };

    //required for Django CSRF protection access
    $.ajaxSetup({
        crossDomain : false,
        beforeSend : function(xhr, settings){
            if (!csrfSafeMethod(settings.type) && !this.crossDomain){
                xhr.setRequestHeader("X-CSRFToken", $.cookie("csrftoken"));
            }
        }
    });

    elems.saveButton.on("click", function (e) {
        console.log("Click");
        e.preventDefault();

        var address = elems.addressInput.val();

        opts.geocoder.geocode({"address": address },
            function (results, status) {
                coordinateLookup(results, status);
            }
        );
    });

    elems.loginButton.on("click", function(e){
        e.preventDefault();
        loginUser();
    })

    elems.deleteButton.on("click", deleteLocation);

    elems.clearButton.on("click", clearFields);

    elems.logoutButton.on("click", logoutUser);

    google.maps.event.addDomListener(window, "load", init);
});